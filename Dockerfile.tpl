FROM python:3.10-bullseye
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR app
COPY ./pymodule/dist/pymodule-[VERSION]-py3-none-any.whl .
RUN pwd
RUN find -iname "*.whl"
RUN pip install pymodule-[VERSION]-py3-none-any.whl
ENTRYPOINT ["operation"]
