#+TITLE: Python Module and Docker Container

This is an example of a python module created with 'poetry' manager,
once all is ok with poetry build the wheel.

#+begin_src bash
poetry build
#+end_src

Then, take the version to run the 'create_docker' script.

#+begin_src bash
bash create_docker.sh VERSION
#+end_src

Then could list images 

#+begin_src bash
sudo docker ps
#+end_src

And run the example.

#+begin_src bash
sudo docker run operation
#+end_src

