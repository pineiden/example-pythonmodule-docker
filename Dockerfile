FROM python:3.10-bullseye
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR app
COPY ./pymodule/dist/pymodule-0.1.0-py3-none-any.whl .
RUN pwd
RUN find -iname "*.whl"
RUN pip install pymodule-0.1.0-py3-none-any.whl
ENTRYPOINT ["operation"]
