VERSION=$1
echo $1

sed "s/\[VERSION\]/${VERSION}/g" Dockerfile.tpl > Dockerfile
echo "New Dockerfile created for ${VERSION}"
echo "Building container"
sudo docker build --progress=plain  --no-cache  -t operation .
