from tasktools.taskloop import TaskLoop
import typer
import asyncio
import logging
from datetime import datetime
from rich import print

app = typer.Typer()

async def operation(*args, **kwargs):
    logger = kwargs["log"]
    now = datetime.utcnow()
    logger.log(10,f"{now} - Asyncio operation....")
    await asyncio.sleep(10)
    return args, kwargs

def get_logger():
    logger = logging.getLogger('test-operation')
    logging.basicConfig()    
    logger.setLevel(logging.DEBUG)
    return logger

@app.command()
def run(mode:bool=False):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    logger = get_logger()
    task = TaskLoop(operation, [], {"log":logger}, loop=loop)
    task_operation = task.create()
    print(f"Running task {task_operation} on loop {loop}")
    loop.run_forever()


if __name__=="__main__":
    app()
